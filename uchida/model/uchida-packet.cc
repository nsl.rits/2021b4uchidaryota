/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2021 Ryota Uchida
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Ryota Uchida <is0398pp@ed.ritsumei.ac.jp>
 *         Ritsumeikan University, Shiga, Japan
 */

//(コメント)パケットの中身を定義しているソースコード
//パケット系のコードはどれも似たようなものなので提案手法によってパケット名,変数,バイト数を変えるだけで大体できる
//(注意事項)packet.ccとpacket.hで対応している変数のバイト数を揃えなければならない


#include "uchida-packet.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"

namespace ns3 {
namespace uchida {

NS_OBJECT_ENSURE_REGISTERED (TypeHeader);

TypeHeader::TypeHeader (MessageType t)
  : m_type (t),
    m_valid (true)
{
}

TypeId
TypeHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::TypeHeader")
    .SetParent<Header> ()
    .SetGroupName ("Uchida")
    .AddConstructor<TypeHeader> ()
  ;
  return tid;
}

TypeId
TypeHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
TypeHeader::GetSerializedSize () const
{
  return 1;
}

void
TypeHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 ((uint8_t) m_type);
}

uint32_t
TypeHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  uint8_t type = i.ReadU8 ();
  m_valid = true;
  switch (type)
    {
    case SAMPLETYPE_RREQ:
    case SAMPLETYPE_RREP:
    case SAMPLETYPE_RERR:
    case SAMPLETYPE_RREP_ACK:
    case HELLOTYPE:
    case SDNTYPE:  //RSUからSDNに送る
    case STRTYPE: //SDNからRSU(ID1)へ
    case RTCTYPE: //RSUからSRCへ
    case BROADCASTTYPE:
    case BROADCAST2TYPE:
      {
        m_type = (MessageType) type;
        break;
      }
    default:
      m_valid = false;
    }
  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
TypeHeader::Print (std::ostream &os) const
{
  switch (m_type)
    {
    case SAMPLETYPE_RREQ:
      {
        os << "RREQ";
        break;
      }
    case SAMPLETYPE_RREP:
      {
        os << "RREP";
        break;
      }
    case SAMPLETYPE_RERR:
      {
        os << "RERR";
        break;
      }
    case SAMPLETYPE_RREP_ACK:
      {
        os << "RREP_ACK";
        break;
      }
      case HELLOTYPE:
      {
         os << "Hello";
        break;
      }     
      case SDNTYPE:
      {
         os << "SDN";
        break;
      }   
      case STRTYPE:
      {
         os << "STR";
        break;
      } 
      case RTCTYPE:
      {
         os << "RTC";
        break;
      } case BROADCASTTYPE:
      {
         os << "BROADCAST";
        break;
      } case BROADCAST2TYPE:
      {
         os << "BROADCAST2";
        break;
      }

    default:
      os << "UNKNOWN_TYPE";
    }
}

bool
TypeHeader::operator== (TypeHeader const & o) const
{
  return (m_type == o.m_type && m_valid == o.m_valid);
}

std::ostream &
operator<< (std::ostream & os, TypeHeader const & h)
{
  h.Print (os);
  return os;
}

//-----------------------------------------------------------------------------
// RREP
//-----------------------------------------------------------------------------

RrepHeader::RrepHeader (uint8_t prefixSize, uint8_t hopCount, Ipv4Address dst,
                        uint32_t dstSeqNo, Ipv4Address origin, Time lifeTime)
  : m_flags (0),
    m_prefixSize (prefixSize),
    m_hopCount (hopCount),
    m_dst (dst),
    m_dstSeqNo (dstSeqNo),
    m_origin (origin)
{
  m_lifeTime = uint32_t (lifeTime.GetMilliSeconds ());
}

NS_OBJECT_ENSURE_REGISTERED (RrepHeader);

TypeId
RrepHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::RrepHeader")
    .SetParent<Header> ()
    .SetGroupName ("Uchida")
    .AddConstructor<RrepHeader> ()
  ;
  return tid;
}

TypeId
RrepHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
RrepHeader::GetSerializedSize () const
{
  return 19;
}

void
RrepHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_flags);
  i.WriteU8 (m_prefixSize);
  i.WriteU8 (m_hopCount);
  WriteTo (i, m_dst);
  i.WriteHtonU32 (m_dstSeqNo);
  WriteTo (i, m_origin);
  i.WriteHtonU32 (m_lifeTime);
}

uint32_t
RrepHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_flags = i.ReadU8 ();
  m_prefixSize = i.ReadU8 ();
  m_hopCount = i.ReadU8 ();
  ReadFrom (i, m_dst);
  m_dstSeqNo = i.ReadNtohU32 ();
  ReadFrom (i, m_origin);
  m_lifeTime = i.ReadNtohU32 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RrepHeader::Print (std::ostream &os) const
{
  os << "destination: ipv4 " << m_dst << " sequence number " << m_dstSeqNo;
  if (m_prefixSize != 0)
    {
      os << " prefix size " << m_prefixSize;
    }
  os << " source ipv4 " << m_origin << " lifetime " << m_lifeTime
     << " acknowledgment required flag " << (*this).GetAckRequired ();
}

void
RrepHeader::SetLifeTime (Time t)
{
  m_lifeTime = t.GetMilliSeconds ();
}

Time
RrepHeader::GetLifeTime () const
{
  Time t (MilliSeconds (m_lifeTime));
  return t;
}

void
RrepHeader::SetAckRequired (bool f)
{
  if (f)
    {
      m_flags |= (1 << 6);
    }
  else
    {
      m_flags &= ~(1 << 6);
    }
}

bool
RrepHeader::GetAckRequired () const
{
  return (m_flags & (1 << 6));
}

void
RrepHeader::SetPrefixSize (uint8_t sz)
{
  m_prefixSize = sz;
}

uint8_t
RrepHeader::GetPrefixSize () const
{
  return m_prefixSize;
}

bool
RrepHeader::operator== (RrepHeader const & o) const
{
  return (m_flags == o.m_flags && m_prefixSize == o.m_prefixSize
          && m_hopCount == o.m_hopCount && m_dst == o.m_dst && m_dstSeqNo == o.m_dstSeqNo
          && m_origin == o.m_origin && m_lifeTime == o.m_lifeTime);
}

void
RrepHeader::SetHello (Ipv4Address origin, uint32_t srcSeqNo, Time lifetime)
{
  m_flags = 0;
  m_prefixSize = 0;
  m_hopCount = 0;
  m_dst = origin;
  m_dstSeqNo = srcSeqNo;
  m_origin = origin;
  m_lifeTime = lifetime.GetMilliSeconds ();
}

std::ostream &
operator<< (std::ostream & os, RrepHeader const & h)
{
  h.Print (os);
  return os;
}


//----------------------------------------------------------------
//HELLO
//----------------------------------------------------------------
HelloHeader::HelloHeader(uint8_t nodeId, uint16_t position_X, uint16_t position_Y)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
HelloHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::HelloHeader")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<HelloHeader> ()
  ;
  return tid;
}

TypeId
HelloHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
HelloHeader::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
  //1bytes + 2bytes + 2bytes= 5bytes
  return 5;
}

void
HelloHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
}

uint32_t
HelloHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
HelloHeader::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y;     
}    

std::ostream &
operator<<(std::ostream & os, HelloHeader const & h)
{
  h.Print(os);
  return os;
}


//----------------------------------------------------------------
//SendSDN
//----------------------------------------------------------------
SDNHeader::SDNHeader(uint8_t nodeId, uint16_t position_X, uint16_t position_Y)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
SDNHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::SDNHeader")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<SDNHeader> ()
  ;
  return tid;
}

TypeId
SDNHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
SDNHeader::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
  //1bytes + 2bytes + 2bytes= 5bytes
  return 5;
}

void
SDNHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
}

uint32_t
SDNHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
SDNHeader::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y;     
}    

std::ostream &
operator<<(std::ostream & os, SDNHeader const & h)
{
  h.Print(os);
  return os;
}




//----------------------------------------------------------------
//SendRTC
//----------------------------------------------------------------
RTCHeader::RTCHeader(uint8_t nodeId, uint16_t position_X, uint16_t position_Y)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
RTCHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::RTCHeader")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<RTCHeader> ()
  ;
  return tid;
}

TypeId
RTCHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
RTCHeader::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
  //1bytes + 2bytes + 2bytes= 5bytes
  return 5;
}

void
RTCHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
}

uint32_t
RTCHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
RTCHeader::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y;     
}    

std::ostream &
operator<<(std::ostream & os, RTCHeader const & h)
{
  h.Print(os);
  return os;
}






//----------------------------------------------------------------
//SendSTR
//----------------------------------------------------------------
STRHeader::STRHeader(uint8_t nodeId, uint16_t position_X, uint16_t position_Y)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
STRHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::STRHeader")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<STRHeader> ()
  ;
  return tid;
}

TypeId
STRHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
STRHeader::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
  //1bytes + 2bytes + 2bytes= 5bytes
  return 5;
}

void
STRHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
}

uint32_t
STRHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
STRHeader::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y;     
}    

std::ostream &
operator<<(std::ostream & os, STRHeader const & h)
{
  h.Print(os);
  return os;
}



//----------------------------------------------------------------
//SendBroadcast
//----------------------------------------------------------------
BROADCASTHeader::BROADCASTHeader(uint8_t nodeId, uint16_t position_X, uint16_t position_Y)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
BROADCASTHeader::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::BROADCASTHeader")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<BROADCASTHeader> ()
  ;
  return tid;
}

TypeId
BROADCASTHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
BROADCASTHeader::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)
  //1bytes + 2bytes + 2bytes= 5bytes
  return 5;
}

void
BROADCASTHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
}

uint32_t
BROADCASTHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
BROADCASTHeader::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y;     
}    

std::ostream &
operator<<(std::ostream & os, BROADCASTHeader const & h)
{
  h.Print(os);
  return os;
}







//----------------------------------------------------------------
//SendBroadcast2
//----------------------------------------------------------------
BROADCAST2Header::BROADCAST2Header(uint8_t nodeId, uint16_t position_X, uint16_t position_Y, uint8_t hopCount)
  :m_nodeId(nodeId),
   m_position_X(position_X),
   m_position_Y(position_Y)
{
}

TypeId
BROADCAST2Header::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::uchida::BROADCAST2Header")
    .SetParent<Header> ()
    .SetGroupName("Uchida")
    .AddConstructor<BROADCAST2Header> ()
  ;
  return tid;
}

TypeId
BROADCAST2Header::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
BROADCAST2Header::GetSerializedSize () const
{
  //8bits(nodeId)+16bits(position_X)+16bits(position_Y)+8bits(hop)
  //1bytes + 2bytes + 2bytes + 1bytes= 6bytes
  return 6;
}

void
BROADCAST2Header::Serialize (Buffer::Iterator i) const
{
  i.WriteU8 (m_nodeId);
  i.WriteU16 (m_position_X);
  i.WriteU16 (m_position_Y);
   i.WriteU8 (m_hopCount);
}

uint32_t
BROADCAST2Header::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  m_nodeId = i.ReadU8 ();
  m_position_X = i.ReadU16 ();
  m_position_Y = i.ReadU16 ();
  m_hopCount = i.ReadU8 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
BROADCAST2Header::Print (std::ostream &os) const
{
  os <<" nodeId " << m_nodeId
     <<" Position_X "<<m_position_X
     <<" Position_Y "<<m_position_Y
     <<" Hop Count " << m_hopCount;     
}    

std::ostream &
operator<<(std::ostream & os, BROADCAST2Header const & h)
{
  h.Print(os);
  return os;
}

}
}
